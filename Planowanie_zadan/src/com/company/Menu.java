package com.company;

import javax.swing.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adam on 2016-01-08.
 */
public class Menu {

    List<Task> today = new ArrayList<>();
    List<Task> tomorrow = new ArrayList<>();
    List<Task> this_week = new ArrayList<>();
    List<Task> this_month = new ArrayList<>();

    public void menu_konsolowe() {
        System.out.println("Witaj w programie!\n" +
                "Dzi�ki programowi w latwy sposob zaplanujesz swoje zadania.\n" +
                "Co chcialbys zrobic?\n");
        int wybor = 0;
        do {
            System.out.println("1.Dodaj zadanie\n" +
                    "2. Ogladaj kategorie\n" +
                    "3. Pokaz zadania niedokonczone\n" +
                    "4. Usun zadanie\n" +
                    "5. Oznacz jako wykonane\n" +
                    "6. Wyjscie\n");
            Scanner in = new Scanner(System.in);
            wybor = in.nextInt();

            switch (wybor) {
                case 1:
                    add();
                    break;
                case 2:
                    watch();
                    break;
                case 3:
                    show_undone();
                    break;
                case 4:
                    remove();
                    break;
                case 5:
                    mark_as_done();
                    break;
                case 6:
                    System.out.println("Wyjscie...");
                    break;
            }

        } while (wybor != 4);

    }

    private void mark_as_done() {
        System.out.println("Uzyj funkcji wyswietl, by zobaczyc numeracje zadan. \n");

        System.out.println("Z ktorej kategorii?\n" +
                "1. Dzisiaj\n" +
                "2. Jutro\n" +
                "3. Ten tydzien\n" +
                "4. Ten miesiac\n");
        Scanner in = new Scanner(System.in);
        int wybor = in.nextInt();

        System.out.println("Jaki jest numer zadania do oznaczenia?");
        int numer = in.nextInt();

        switch (wybor){
            case 1:
                today.get(numer).setDone();
                break;
            case 2:
                tomorrow.get(numer).setDone();
                break;
            case 3:
                this_week.get(numer).setDone();
                break;
            case 4:
                this_month.get(numer).setDone();
                break;
        }
    }

    public void add() {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj priorytet zadania (1-10):\n");
        int prior=5;
        try {
            prior = in.nextInt();
            in.nextLine();
        }catch (InputMismatchException e)
        {
            System.out.println(".");
        }
        System.out.println("Podaj tresc zadania:\n");
        String tresc = "";
        try {
            tresc = in.nextLine();
        }catch (InputMismatchException e)
        {
            System.out.println(".");
        }
        System.out.println("W trakcie wykonywania?\n");
        boolean w_trakcie = false;
        try {
            w_trakcie = in.nextBoolean();
        }catch (InputMismatchException e)
        {
            System.out.println(".");
        }

        System.out.println("Wybierz kategori�:\n" +
                "1. Dzisiaj\n" +
                "2. Jutro\n" +
                "3. Ten tydzien\n" +
                "4. Ten miesiac\n");

        int kategoria = in.nextInt();

        if (kategoria == 1){

            today.add(new Task(w_trakcie,tresc,prior));
        }
        else if (kategoria == 2){

            tomorrow.add(new Task(w_trakcie,tresc,prior));
        }
        else if (kategoria == 3){

            this_week.add(new Task(w_trakcie,tresc,prior));
        }
        else if (kategoria == 4){

            this_month.add(new Task(w_trakcie,tresc,prior));
        }

    }

    public void watch() {
        System.out.println("Ktora kategorie chcesz obejrzec?\n" +
                "1. Dzisiaj\n" +
                "2. Jutro\n" +
                "3. Ten tydzien\n" +
                "4. Ten miesiac\n");
        Scanner in = new Scanner(System.in);
        int wybor = in.nextInt();

        switch (wybor){
            case 1:
                for (int i=0;i<today.size();i++){
                    System.out.println("Zadanie numer: " + i+1 + "\n");
                    today.get(i).show();
                }
                break;
            case 2:
                for (int i=0;i<tomorrow.size();i++){
                    System.out.println("Zadanie numer: " + i+1 + "\n");
                    tomorrow.get(i).show();
                }
                break;
            case 3:
                for (int i=0;i<this_week.size();i++){
                    System.out.println("Zadanie numer: " + i+1 + "\n");
                    this_week.get(i).show();
                }
                break;
            case 4:
                for (int i=0;i<this_month.size();i++){
                    System.out.println("Zadanie numer: " + i+1 + "\n");
                    this_month.get(i).show();
                }
                break;
        }
    }

    public void show_undone() {
        List<Task> undone = new ArrayList<>();

        for(int i=0;i<today.size();i++)
        {
            if(!today.get(i).getDone())
                undone.add(today.get(i));
        }

        for(int i=0;i<tomorrow.size();i++)
        {
            if(!tomorrow.get(i).getDone())
                undone.add(tomorrow.get(i));
        }

        for(int i=0;i<this_week.size();i++)
        {
            if(!this_week.get(i).getDone())
                undone.add(this_week.get(i));
        }

        for(int i=0;i<this_month.size();i++)
        {
            if(!this_month.get(i).getDone())
                undone.add(this_month.get(i));
        }

        for (int i=0;i<undone.size();i++){
            System.out.println("Zadanie numer: " + i+1 + "\n");
            undone.get(i).show();
        }


    }

    public void remove(){
        System.out.println("Uzyj funkcji wyswietl, by zobaczyc numeracje zadan. \n" +
                "");

        System.out.println("Z ktorej kategorii usuwamy?\n" +
                "1. Dzisiaj\n" +
                "2. Jutro\n" +
                "3. Ten tydzien\n" +
                "4. Ten miesiac\n");
        Scanner in = new Scanner(System.in);
        int wybor = in.nextInt();

        System.out.println("Jaki jest numer zadania do usuniecia?");
        int numer = in.nextInt();

        switch (wybor){
            case 1:
                today.remove(numer-1);
                break;
            case 2:
                tomorrow.remove(numer-1);
                break;
            case 3:
                this_week.remove(numer-1);
                break;
            case 4:
                this_month.remove(numer-1);
                break;
        }
    }

    public void menu_graficzne(){
        Tasker okno = new Tasker();
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        okno.setVisible(true);
    }
}

