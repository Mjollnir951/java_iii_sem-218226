package com.company;

/**
 * Created by Adam on 2015-11-29.
 */
public class Task {
    private boolean done;
    private boolean in_progress;
    private String content;
    private int priority;

    public Task(boolean w_trak, String zawart, int wazn)
    {
        w_trak = in_progress;
        content = zawart;
        wazn = priority;
    }

    public void show(){
        System.out.println("Waznosc: " + priority + "\n" +
                "W trakcie: " + in_progress + "\n" +
                "Zakonczone: " + done + "\n" +
                "Tresc: " + content + "\n");
    }

    public boolean get_status()
    {
        return in_progress;
    }

    public String getContent(){
        return content;
    }

    public int getPriority(){
        return priority;
    }

    public boolean getDone()
    {
        return done;
    }

    public void setDone(){done = true;}


}
