package com.company;

import com.sun.javafx.tk.Toolkit;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adam on 2016-01-10.
 */
public class Tasker extends JFrame implements ActionListener {

    private static JButton bdodaj, busun;
    private static JTextArea ttasks, tpr, tdon, tin;
    private static JComboBox kategoria;
    private static JLabel ltasks, lprior, ldone, lin_prog;
    private static JTable name;
    public static Menu listy = new Menu();

    public Tasker() {
        setSize(500, 500);
        setTitle("Tasker");
        setLayout(null);

        bdodaj = new JButton("Dodaj");
        bdodaj.setBounds(380, 50, 100, 25);
        add(bdodaj);
        bdodaj.addActionListener(this);

        busun = new JButton("Usun");
        busun.setBounds(380, 80, 100, 25);
        add(busun);
        busun.addActionListener(this);

        ttasks = new JTextArea();
        JScrollPane scr = new JScrollPane(ttasks);
        scr.setBounds(20, 80, 190, 350);
        add(scr);

        tpr = new JTextArea();
        JScrollPane scr1 = new JScrollPane(tpr);
        scr1.setBounds(210, 80, 50, 350);
        add(scr1);

        tin = new JTextArea();
        JScrollPane scr3 = new JScrollPane(tin);
        scr3.setBounds(325, 80, 50, 350);
        add(scr3);

        tdon = new JTextArea();
        JScrollPane scr2 = new JScrollPane(tdon);
        scr2.setBounds(265, 80, 50, 350);
        add(scr2);

        ltasks = new JLabel("Zadania");
        ltasks.setBounds(50, 60, 80, 25);
        add(ltasks);
        busun.addActionListener(this);

        lprior = new JLabel("Priorytet |");
        lprior.setBounds(205, 60, 70, 25);
        add(lprior);

        lin_prog = new JLabel("W trakcie |");
        lin_prog.setBounds(260, 60, 70, 25);
        add(lin_prog);

        ldone = new JLabel("Zakoncz");
        ldone.setBounds(324, 60, 70, 25);
        add(ldone);


        kategoria = new JComboBox();
        kategoria.setBounds(20, 20, 330, 25);
        add(kategoria);
        kategoria.addItem("Wybierz");
        kategoria.addItem("Dzisiaj");
        kategoria.addItem("Jutro");
        kategoria.addItem("Ten tydzien");
        kategoria.addItem("Ten miesiac");
        kategoria.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == bdodaj) {
            Add dodaj = new Add();
            dodaj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            dodaj.setVisible(true);
        } else if (source == busun) {
            Delete usun = new Delete();
            usun.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            usun.setVisible(true);
        } else if (source == kategoria)
            switch (kategoria.getSelectedIndex()) {
                case 0:
                    break;
                case 1:
                    for (int i = 0; i < listy.today.size(); i++) {
                        ttasks.setText(listy.today.get(i).getContent());
                        tpr.setText(Integer.toString(listy.today.get(i).getPriority()) + "\n");
                        tin.setText(Boolean.toString(listy.today.get(i).get_status()) + "\n");
                        tdon.setText(Boolean.toString(listy.today.get(i).getDone()) + "\n");
                    }
                    break;
                case 2:
                    for (int i = 0; i < listy.tomorrow.size(); i++) {
                        ttasks.setText(listy.tomorrow.get(i).getContent());
                        tpr.setText(Integer.toString(listy.tomorrow.get(i).getPriority()) + "\n");
                        tin.setText(Boolean.toString(listy.tomorrow.get(i).get_status()) + "\n");
                        tdon.setText(Boolean.toString(listy.tomorrow.get(i).getDone()) + "\n");
                    }
                    break;
                case 3:
                    for (int i = 0; i < listy.this_week.size(); i++) {
                        ttasks.setText(listy.this_week.get(i).getContent());
                        tpr.setText(Integer.toString(listy.this_week.get(i).getPriority()) + "\n");
                        tin.setText(Boolean.toString(listy.this_week.get(i).get_status()) + "\n");
                        tdon.setText(Boolean.toString(listy.this_week.get(i).getDone()) + "\n");
                    }
                    break;
                case 4:
                    for (int i = 0; i < listy.this_month.size(); i++) {
                        ttasks.setText(listy.this_month.get(i).getContent());
                        tpr.setText(Integer.toString(listy.this_month.get(i).getPriority()) + "\n");
                        tin.setText(Boolean.toString(listy.this_month.get(i).get_status()) + "\n");
                        tdon.setText(Boolean.toString(listy.this_month.get(i).getDone()) + "\n");
                    }
                    break;

            }

    }
}
    /**
     * Created by Adam on 2016-01-10.
     */
    class Add extends JFrame implements ActionListener {

        //klasa dla nowego okna, dodawania nowego zadania

        private JButton bdodaj;
        private JLabel lw_trak,lprior;
        private JTextArea ttasks,tprior;
        private String tresc;
        private int prior;
        private boolean w_trak;
        private JCheckBox chprior;
        private JComboBox kategoria;

        public Add(){
            setSize(280,450);
            setTitle("Tasker");
            setLayout(null);
            bdodaj = new JButton("Dodaj");
            bdodaj.setBounds(80, 350, 100, 25);
            add(bdodaj);
            bdodaj.addActionListener(this);

            ttasks = new JTextArea("Wpisz tresc...");
            JScrollPane scroll = new JScrollPane(ttasks);
            ttasks.setAlignmentY(TOP_ALIGNMENT);
            scroll.setBounds(20,50,200,200);
            add(scroll);

            tprior = new JTextArea("0");
            tprior.setAlignmentY(TOP_ALIGNMENT);
            tprior.setBounds(20, 285, 200, 30);
            add(tprior);

            lw_trak = new JLabel("W trakcie? :");
            lw_trak.setBounds(80,315,100,30);
            add(lw_trak);

            lprior = new JLabel("Wpisz priorytet (1-10):");
            lprior.setBounds(20,255,150,25);
            add(lprior);

            chprior = new JCheckBox();
            chprior.setBounds(150,320,25,25);
            add(chprior);
            chprior.addActionListener(this);

            kategoria = new JComboBox();
            kategoria.setBounds(20,20,200,25);
            add(kategoria);
            kategoria.addItem("Wybierz");
            kategoria.addItem("Dzisiaj");
            kategoria.addItem("Jutro");
            kategoria.addItem("Ten tydzien");
            kategoria.addItem("Ten miesiac");
            kategoria.addActionListener(this);

        }


        @Override
        public void actionPerformed(ActionEvent e) {
            tresc = ttasks.getText();
            prior = Integer.parseInt(tprior.getText());
            w_trak = chprior.isSelected();

                        Object source = e.getSource();
                        Tasker tas = new Tasker();

                        if(source == bdodaj) {
                            switch (kategoria.getSelectedIndex()) {
                                case 0:
                                    dispose();
                                    break;
                                case 1:
                                    tas.listy.today.add(new Task(w_trak, tresc, prior));
                                    this.dispose();
                                    break;
                                case 2:
                                    tas.listy.tomorrow.add(new Task(w_trak, tresc, prior));
                                    this.dispose();
                                    break;
                                case 3:
                                    tas.listy.this_week.add(new Task(w_trak, tresc, prior));
                        this.dispose();
                        break;
                    case 4:
                        tas.listy.this_month.add(new Task(w_trak, tresc, prior));
                        this.dispose();
                        break;
                }
            }
        }
    }

    /**
     * Created by Adam on 2016-01-10.
     */
    class Delete extends JFrame implements ActionListener {

        JButton busun;
        JComboBox kategoria;
        JTextField ktory;
        JLabel kom;

        public Delete() {
            setSize(280, 250);
            setTitle("Tasker");
            setLayout(null);

            busun = new JButton("Usun");
            busun.setBounds(80, 170, 100, 25);
            add(busun);
            busun.addActionListener(this);

            kom = new JLabel("Wpisz numer zadania do usuniecia");
            kom.setBounds(20,100,250,25);
            add(kom);

            ktory = new JTextField("0");
            ktory.setBounds(20,130,200,25);
            add(ktory);

            kategoria = new JComboBox();
            kategoria.setBounds(20,20,200,25);
            add(kategoria);
            kategoria.addItem("Wybierz");
            kategoria.addItem("Dzisiaj");
            kategoria.addItem("Jutro");
            kategoria.addItem("Ten tydzien");
            kategoria.addItem("Ten miesiac");
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int numer = Integer.parseInt(ktory.getText());
            Tasker tsk = new Tasker();

            switch (kategoria.getSelectedIndex()){
                case 0:
                    this.dispose();
                    break;
                case 1:
                    tsk.listy.today.remove(numer-1);
                    this.dispose();
                    break;
                case 2:
                    tsk.listy.tomorrow.remove(numer-1);
                    this.dispose();
                    break;
                case 3:
                    tsk.listy.this_week.remove(numer-1);
                    this.dispose();
                    break;
                case 4:
                    tsk.listy.this_month.remove(numer-1);
                    this.dispose();
                    break;
            }
        }
    }

