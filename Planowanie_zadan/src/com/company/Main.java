package com.company;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner in = new Scanner(System.in);

        System.out.println("Uruchomic tryb graficzny?(true/false)");
        boolean graph_mode = false;
        try {
            graph_mode = in.nextBoolean();
        }catch (InputMismatchException e)
        {
            System.out.println("Tryb konsoli...");
        }

        Menu menu = new Menu();

        if (graph_mode) menu.menu_graficzne();
        else menu.menu_konsolowe();

    }
}
