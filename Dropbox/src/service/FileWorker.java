package service;

import com.dropbox.core.DbxClient;

import java.util.concurrent.Callable;

/**
 * Created by Pawel on 2014-12-05.
 */
public class FileWorker implements Callable{

    private FileSender fileSender;
    private DbxClient dbxClient;
    String from;
    String to;
    public FileWorker(FileSender fileSender, String from, String to, DbxClient dbxClient) {
        this.fileSender = fileSender;
        this.from = from;
        this.to = to;
        this.dbxClient = dbxClient;
    }

    @Override
    public Void call() throws Exception {
        fileSender.send(from, to, dbxClient);
        return null;
    }
}
