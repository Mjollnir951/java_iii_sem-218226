package service;

import com.dropbox.core.*;
import com.dropbox.core.util.IOUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: pszczerbicki
 */
public class FileSender {

    AtomicInteger stats = new AtomicInteger(0);

    public void send(String from, String to, DbxClient dbxClient) {
        sendFile(Paths.get(from), to, dbxClient);
        stats.incrementAndGet();
    }

    public int getStats() {
        return stats.getAndSet(0);
    }

    private synchronized void sendFile(Path from, String to, DbxClient dbxClient) {
        try {
            //TODO send file

            if (!Files.isReadable(from))
                Thread.sleep(200);

            Path toPath = Paths.get(to, from.getFileName().toString());

            if(!Files.exists(toPath)) {
                Files.copy(from, toPath);
                System.out.println("new file: " + from.getFileName());

                DbxEntry.File metadata;
                try {
                    InputStream in = new FileInputStream(from.toString());
                    try {
                        metadata = dbxClient.uploadFile("/" + from.getFileName(), DbxWriteMode.add(), -1, in);
                    } catch (DbxException ex) {
                        System.out.println("Error uploading to Dropbox: " + ex.getMessage());
                    } finally {
                        IOUtil.closeInput(in);
                        System.out.println("Upload to Dropbox was Successful!");
                    }
                } catch (IOException ex) {
                    System.out.println("Error reading from file \"" + from.getFileName() + "\": " + ex.getMessage());
                }
            }else{
                System.out.println("already existing file: " + from.getFileName());
            }
        }catch(Exception e){
            System.out.println("SenderError: " + e.toString());
        }
    }


}
