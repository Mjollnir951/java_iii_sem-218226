import properties.ConfigService;
import service.FileSender;
import worker.FileListener;

import com.dropbox.core.*;

import java.io.*;
import java.nio.file.*;
import java.util.Locale;

/**
 * Created by Adam on 25.01.2016.
 */
public class Main {
    public static void main(String[] args) throws IOException, DbxException {
        final String APP_KEY = "rsrmd7u8ed1uwrp";
        final String APP_SECRET = "rsrmd7u8ed1uwrp";

        ConfigService config = null;
        try {
            config = new ConfigService("config.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileSender sender = new FileSender();

        int threads = Integer.parseInt(config.getProp("THREADS_AMOUNT"));
        System.out.println("Threads: " + threads);

        DbxAppInfo appInfo = new DbxAppInfo(config.getProp("APP_KEY"), config.getProp("APP_SECRET"));

        DbxRequestConfig dbxConfig = new DbxRequestConfig("JavaTutorial/1.0",
                Locale.getDefault().toString());
        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(dbxConfig, appInfo);

        // Have the user sign in and authorize your app.
        String authorizeUrl = webAuth.start();
        System.out.println("1. Go to: " + authorizeUrl);
        System.out.println("2. Click \"Allow\" (you might have to log in first)");
        System.out.println("3. Copy the authorization code and paste it below: ");
        String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();

        // This will fail if the user enters an invalid authorization code.
        DbxAuthFinish authFinish = webAuth.finish(code);
        String accessToken = authFinish.accessToken;

        DbxClient dbxClient = new DbxClient(dbxConfig, accessToken);

        System.out.println("Linked account: " + dbxClient.getAccountInfo().displayName);

        FileListener x = new FileListener(config, sender, threads, dbxClient);
        x.firstRun(Paths.get(config.getProp("FROM")), config.getProp("TO"));
        while (true) {
            x.listen(Paths.get(config.getProp("FROM")), config.getProp("TO"));
//            System.out.println(sender.getStats());
        }
    }
}
