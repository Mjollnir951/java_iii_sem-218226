package worker;

import com.dropbox.core.DbxClient;
import properties.ConfigService;
import service.FileSender;
import service.FileWorker;

import java.io.File;
import java.nio.file.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * Created by Pawel on 2014-12-05.
 */
public class FileListener {

    private ConfigService configService;

    private FileSender sender;

    private ExecutorService pool;

    private DbxClient dbxClient;

    public FileListener(ConfigService configService, FileSender sender, int threadAmount, DbxClient dbxClient) {
        this.configService = configService;
        this.sender = sender;
        this.dbxClient = dbxClient;
        pool = Executors.newFixedThreadPool(threadAmount);
    }

    public void checkDirs(Path myDir, String toDir) {
        try {
            if (!Files.exists(myDir))
                Files.createDirectory(myDir);

            if (!Files.exists(Paths.get(toDir)))
                Files.createDirectory(Paths.get(toDir));

        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public void firstRun(Path myDir, String toDir) {
        checkDirs(myDir, toDir);
        File file = myDir.toFile();
        final File[] files = file.listFiles();
        if (files != null) {
            for (File child : files) {
                if (child.isFile())
                    sendInThread(child.toString(), toDir);
            }
        }

    }

    public void listen(Path myDir, String toDir) {
        checkDirs(myDir, toDir);

        try {
            WatchService watcher = myDir.getFileSystem().newWatchService();
            myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE);

            WatchKey watchKey = watcher.take();

            List<WatchEvent<?>> events = watchKey.pollEvents();
            for (WatchEvent event : events) {
                if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                    Path modifiedPath = myDir.resolve((Path) event.context());
//                    System.out.println("Path " + modifiedPath + " modified EVENT.");
                    sendInThread(modifiedPath.toString(), toDir);
                }
            }

        } catch (NotDirectoryException e) {
            System.out.println("Not a directory: " + e.toString());
            System.exit(1);
        } catch (Exception e){
            System.out.println("ListenerError: " + e.toString());
            System.out.println(Thread.currentThread().getStackTrace()[2].getLineNumber());
            System.exit(1);
        }
    }

    public void sendInThread(String from, String to){
        try {
            pool.submit(new FileWorker(sender, from, to, dbxClient));
        }catch(Exception e) {
            e.printStackTrace();
        }
//        pool.shutdown();
//            this.listen(Paths.get(from));
    }
}
