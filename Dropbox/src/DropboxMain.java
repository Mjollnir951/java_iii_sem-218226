// Include the Dropbox SDK.

import com.dropbox.core.*;

import java.io.*;
import java.util.Locale;

public class DropboxMain {
    public static void main(String[] args) throws IOException, DbxException {
        // Get your app key and secret from the Dropbox developers website.
        final String APP_KEY = "rsrmd7u8ed1uwrp";
        final String APP_SECRET = "rsrmd7u8ed1uwrp";

        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

        DbxRequestConfig dbxConfig = new DbxRequestConfig("JavaTutorial/1.0",
                Locale.getDefault().toString());
        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(dbxConfig, appInfo);

        // Have the user sign in and authorize your app.
        String authorizeUrl = webAuth.start();
        System.out.println("1. Go to: " + authorizeUrl);
        System.out.println("2. Click \"Allow\" (you might have to log in first)");
        System.out.println("3. Copy the authorization code.");
        String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();

        // This will fail if the user enters an invalid authorization code.
        DbxAuthFinish authFinish = webAuth.finish(code);
        String accessToken = authFinish.accessToken;

        DbxClient client = new DbxClient(dbxConfig, accessToken);

        System.out.println("Linked account: " + client.getAccountInfo().displayName);

        DbxEntry.WithChildren listing = client.getMetadataWithChildren("/");
        System.out.println("Files in the root path:");
        if (listing == null) {
            System.out.println("No file or folder at that path.");
        }else{
            System.out.println("There are some files, trying to get them...");
            for (DbxEntry child : listing.children) {
                System.out.println("	" + child.name + ": " + child.toString());
            }
            System.out.println(listing.toStringMultiline());
        }
    }
}
