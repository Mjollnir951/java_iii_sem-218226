package com.company;

public class Main {

    public static void main(String[] args) {
        //pobranie konfiguracji
	    ConfigFileReader configFileReader = new ConfigFileReader("config.cfg");
        ConfigData.Instance.setSettings(configFileReader.Read());

        //zmienne do nazw
        String ext = ".CSV";
        String fileName = ConfigData.Instance.getFileName()+ext;
        String outputFileName = ConfigData.Instance.getOutputFileName()+ext;
        String errorFileName = ConfigData.Instance.getErrorFileName()+ext;

        //wczytanie danych
        CSVPersonFileReader csvPersonFileReader = new CSVPersonFileReader(fileName);
        PersonDataManager personDataManager = new PersonDataManager();
        personDataManager.LoadPersonArrayList(csvPersonFileReader.Read());

        //walidacja
        personDataManager.ValidateData();

        //zapis
        PersonDataToFileSaver personDataToFileSaver = new PersonDataToFileSaver(outputFileName,errorFileName,ConfigData.Instance.getRowsPerFile(),personDataManager.getPersonData(),personDataManager.getErrors());
        personDataToFileSaver.Save();
    }
}
