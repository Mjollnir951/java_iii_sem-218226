package com.company;

import com.company.Person;
import com.company.PersonError;
import com.company.PeselValidator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Adam on 27.01.2016.
 */
public class PersonDataManager {
    HashSet<Person> persons = new HashSet<Person>();
    ArrayList<PersonError> errors = new ArrayList<PersonError>();

    public void LoadPersonArrayList(ArrayList<Person> personArrayList){
        persons.clear();
        for(Person person : personArrayList){
            persons.add(person);
        }
    }

    public void ValidateData(){
        ArrayList<Person> toRemove = new ArrayList<Person>();
        for (Person person : persons) {
            if(!isPeselValid(person)){
                errors.add(generatePersonError(person, "PESEL niepoprawny"));
                toRemove.add(person);
            }
        }
        persons.removeAll(toRemove);
    }

    protected PersonError generatePersonError(Person person, String errMsg){
        return new PersonError(person, errMsg);
    }

    protected boolean isPeselValid(Person person){
        PeselValidator peselValiodator = new PeselValidator();
        return peselValiodator.Validate(person.Pesel);
    }

    public ArrayList<Person> getPersonData(){
        ArrayList<Person> personArrayList = new ArrayList<Person>();
        for(Person person : persons){
            personArrayList.add(person);
        }
        return personArrayList;
    }

    public ArrayList<PersonError> getErrors(){
        return errors;
    }
}
