package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adam on 27.01.2016.
 */
public abstract class LineFileReader<T> {
    protected List<String> lines;

    protected String fileName;

    LineFileReader(String fileName){
        this.fileName = fileName;
    }

    public void SetFileName(String fileName){
        this.fileName = fileName;
    }

    protected void ReadFile(){
        try {
            lines = Files.readAllLines(Paths.get(fileName), Charset.forName("UTF-8"));
        } catch (IOException e) {
            lines = null;
        }
    }

    protected abstract T convertDataToExport();

    public T Read(){
        ReadFile();
        return convertDataToExport();
    }
}
