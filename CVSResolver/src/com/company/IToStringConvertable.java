package com.company;

/**
 * Created by Adam on 27.01.2016.
 */
public interface IToStringConvertable {
    public String ConvertToString();
}
