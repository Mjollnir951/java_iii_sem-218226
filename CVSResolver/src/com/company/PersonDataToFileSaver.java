package com.company;

import java.util.ArrayList;

/**
 * Created by Adam on 27.01.2016.
 */
public class PersonDataToFileSaver {
    private String outputName;
    private String errorName;
    private String extension = "CSV";
    private int rows;

    private ArrayList<Person> persons;
    private ArrayList<PersonError> errors;

    public PersonDataToFileSaver(String outputFilename, String errorFileName, int rowNumber, ArrayList<Person> persons, ArrayList<PersonError> errors){
        outputName = outputFilename;
        errorName = errorFileName;
        rows = rowNumber;
        this.persons = persons;
        this.errors = errors;
    }

    public void Save(){
        SaveErrors();
        SaveDataInRows(ConvertToRows());
    }

    protected void SaveErrors(){
        PersonFileSaver<PersonError> errorSaver = new PersonFileSaver<PersonError>(errors, errorName+"."+extension);
        errorSaver.Save();
    }

    protected void SaveDataInRows(ArrayList<ArrayList<Person>> data){
        int counter = 0;
        PersonFileSaver<Person> personSaver = new PersonFileSaver<Person>();
        for(ArrayList<Person> persons : data){
            personSaver.SetFileName(outputName+"_"+counter+"_"+persons.size()+"rows."+extension);
            personSaver.SetArrayList(persons);
            personSaver.Save();
            counter++;
        }
    }

    protected ArrayList<ArrayList<Person>> ConvertToRows(){
        ArrayList<ArrayList<Person>> result = new ArrayList<ArrayList<Person>>();
        ArrayList<Person> temp = new ArrayList<Person>();
        int count = 0;
        for(Person person : persons){
            temp.add(person);
            if(++count == rows){
                count = 0;
                result.add(temp);
                temp = new ArrayList<Person>();
            }
        }
        if(count!=0) {
            result.add(temp);
        }

        return result;
    }
}
