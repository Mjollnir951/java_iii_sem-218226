package com.company;

/**
 * Created by Adam on 27.01.2016.
 */
public class PersonError implements IToStringConvertable{
    public Person person;
    public String errorMsg;

    public PersonError(Person person, String errorMsg){
        this.person = person;
        this.errorMsg = errorMsg;
    }

    public String ConvertToString(){
        return person.ConvertToString() + "\t:\t" + errorMsg;
    }
}
