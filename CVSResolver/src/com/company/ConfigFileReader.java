package com.company;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Adam on 27.01.2016.
 */
public class ConfigFileReader extends LineFileReader<HashMap<String, String>>{

    public ConfigFileReader(String fileName){
        super(fileName);
    }

    @Override
    protected HashMap<String, String> convertDataToExport() {
        HashMap<String, String> settings = new HashMap<String, String>();
        for(String line:lines){
            String[] lineData = line.split("=");
            settings.put(lineData[0].trim(), lineData[1].trim());
        }
        return settings;
    }
}
