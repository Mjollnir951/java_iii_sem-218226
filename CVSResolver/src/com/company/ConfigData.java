package com.company;

import java.util.HashMap;

/**
 * Created by Adam on 27.01.2016.
 */
public class ConfigData {

    private HashMap<String, String> settings = new HashMap<String, String>();

    public static ConfigData Instance = new ConfigData();

    private ConfigData(){}

    public void setSettings(HashMap<String, String> settings){
        this.settings = settings;
    }

    public String getFileName(){
        return  settings.get("filename");
    }

    public String getOutputFileName(){
        return settings.get("outputFilename");
    }

    public String getErrorFileName(){
        return settings.get("errorsFilename");
    }

    public int getRowsPerFile(){
        return Integer.parseInt(settings.get("rowsPerFile"));
    }
}
