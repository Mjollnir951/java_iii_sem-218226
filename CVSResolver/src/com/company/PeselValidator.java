package com.company;

/**
 * Created by Adam on 27.01.2016.
 */
public class PeselValidator {

    public boolean Validate(String pesel){
        return isPeselLengthValid(pesel) ? isCheckSumValid(pesel) : false;
    }

    protected boolean isCheckSumValid(String pesel){

        char[] peselElem = pesel.toCharArray();

        int[] weigth = new int[] {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

        int sum = 0;
        for (int i = 0; i < peselElem.length-1; i++){
            sum += weigth[i] + Integer.parseInt(""+peselElem[i]);
        }

        int checkSum = sum % 10;

        return checkSum == 0 ? peselElem[10] == '0' : Integer.parseInt(""+peselElem[10]) == (10 - checkSum);
    }

    protected boolean isPeselLengthValid(String pesel){
        return pesel.length() == 11;
    }
}
