package com.company;

import java.util.ArrayList;


/**
 * Created by Adam on 27.01.2016.
 */
public class CSVPersonFileReader extends LineFileReader<ArrayList<Person>>{

    public CSVPersonFileReader(String fileName){
        super(fileName);
    }

    @Override
    protected ArrayList<Person> convertDataToExport() {
        ArrayList<Person> persons = new ArrayList<Person>();
        for(String personString : lines){
            Person person = Person.Parse(personString);
            persons.add(person);
        }
        return persons;
    }

}
