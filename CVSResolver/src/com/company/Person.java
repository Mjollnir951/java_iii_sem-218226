package com.company;

/**
 * Created by Adam on 27.01.2016.
 */
public class Person implements IToStringConvertable{
    public int ID;
    public String Imie;
    public String Nazwisko;
    public String Pesel;

    public Person(int id, String imie, String nazwisko, String pesel){
        ID = id;
        Imie = imie;
        Nazwisko = nazwisko;
        Pesel = pesel;
    }

    public String ConvertToString(){
        return String.format("%d, %s, %s, %s", ID, Imie, Nazwisko, Pesel);
    }

    public static Person Parse(String person){
        String[] personArray = person.split(",");
        return new Person(Integer.parseInt(personArray[0].trim()), personArray[1].trim(), personArray[2].trim(), personArray[3].trim());
    }

    @Override
    public int hashCode() {
        return ID;
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode() == obj.hashCode();
    }
}
