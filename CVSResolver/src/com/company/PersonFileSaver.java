package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by Adam on 25.01.2016.
 */
public class PersonFileSaver<T extends IToStringConvertable> {
    private ArrayList<T> persons;

    private String fileName;

    public PersonFileSaver(){
        this(null, "File.CSV");
    }

    public PersonFileSaver(ArrayList<T> persons, String fileName){
        this.persons = persons;
        this.fileName = fileName;
    }

    public void SetFileName(String fileName){
        this.fileName = fileName;
    }

    public void SetArrayList(ArrayList<T> persons){
        this.persons = persons;
    }

    private ArrayList<String> ConvertToStringList(){
        ArrayList<String> stringArrayList = new ArrayList<String>();

        for(T person : persons){
            stringArrayList.add(person.ConvertToString());
        }

        return stringArrayList;
    }

    public void Save(){
        Path file = Paths.get(fileName);
        try {
            Files.write(file, ConvertToStringList(), Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println("Saving " + fileName + "file failed.");
        }
    }
}
