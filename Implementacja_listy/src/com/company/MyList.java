package com.company;

import java.util.Arrays;

/**
 * Created by Adam on 2015-11-29.
 */
public class MyList {
    private int size = 0;       //pole do zachowania warto�ci obecnego rozmiaru
    private Object[] elements;      //pole-tablica obecnych element�w

    public MyList(){
        elements = new Object[10];
    }

    public void add(Object obj){        // metoda dodaj�ca elementy do listy
        if(elements.length-size <= 5){
            increaseSize();
        }
        elements[size++] = obj;
    }

    public Object get(int index){       // metoda pobieraj�ca element z listy
        if(index<size) {
            return elements[index];
        }else{
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public Object set(int index, Object obj){       //ustawianie warto�ci na konkretnym indexie
        if(index<size){
            elements[index] = obj;
            return obj;
        }else throw new ArrayIndexOutOfBoundsException();
    }

    public Object remove(int index){            // usuwanie elementu o konkretnym indexie
        if (index>=size) {
            throw new ArrayIndexOutOfBoundsException();
        }
            Object delobj = elements[index];

        for (int i = index; i<=size;i++){
            elements[i]=elements[i+1];
        }

        elements[size--]=null;

        return delobj;
    }

    public int size(){              //pobieranie aktualnej warto�ci rozmiaru
        return size;
    }

    public boolean isEmpty(){       //czy lista jest obecnie pusta ?
        if(size==0) return true;
        else return false;
    }

    public boolean contains(Object obj){        // czy lista zawiera konkretny element ?
        for(Object o:elements) {
            if(o==obj) return true;
        }
        return false;
    }

    private void increaseSize() {           // zwi�kszenie rozmiaru (dost�p tylko prywatny !)
        elements = Arrays.copyOf(elements, elements.length *2);
        System.out.println("\nNowa wartosc: " + elements.length);
    }





}
