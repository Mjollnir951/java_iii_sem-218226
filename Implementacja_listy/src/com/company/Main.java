package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        MyList ex = new MyList();       //sprawdzamy poprawność napisanych metod, przykładowa lista i operacje na niej

        ex.add(new Integer(2));
        ex.add(new Integer(5));
        ex.add(new Integer(32));
        ex.add(new Integer(14));
        ex.add(new Integer(9));
        ex.remove(2);
        ex.set(1,new Integer(102));

        System.out.println("Wypiszemy elementy:\n");
        for(int i=0;i<ex.size();i++) {
            System.out.println(ex.get(i) + " ");
        }



    }
}
