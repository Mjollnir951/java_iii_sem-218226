package com.company;

/**
 * Created by Adam on 25.01.2016.
 */
public interface IGenerator<T> {
    public T GenerateNext();
}
