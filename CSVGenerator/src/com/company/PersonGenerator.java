package com.company;

import java.util.Random;

/**
 * Created by Adam on 25.01.2016.
 */
public class PersonGenerator implements IGenerator<Person> {
    private static int ID = 0;
    protected Random _randomGenerator = new Random();

    public Person GenerateNext(){
        return new Person(++ID, GenerateName(), GenerateSurname(), GeneratePesel());
    }

    public int getCurrentID(){
        return ID;
    }

    public void setCurrentID(int ID){
        this.ID = ID;
    }

    protected String GenerateName(){
         String[] namesArray = new String[]{
                 "Mateusz",
                 "Adam",
                 "Micha�",
                 "Michalina",
                 "Iga",
                 "Nela",
                 "Bartosz",
                 "Andrzej",
                 "Rafa�",
                 "Iza",
                 "Zuzanna",
                 "Geralt",
                 "Zbigniew",
                 "Robert",
                 "Anita",
                 "Magdalena",
                 "Martyna",
                 "Adrianna"};

        return namesArray[_randomGenerator.nextInt(namesArray.length)];
    }

    protected String GenerateSurname() {
        String[] surnamesArray = new String[]{
                "Kempa",
                "Baran",
                "Adamkiewicz",
                "Majda",
                "Nowak",
                "Kowal",
                "Pasibrzoza",
                "Radziwi�",
                "Plichta",
                "Pluta",
                "Rado�",
                "Skowro�",
                "Skrobek"
        };

        return surnamesArray[_randomGenerator.nextInt(surnamesArray.length)];
    }

    protected String GeneratePesel(){
        int year = _randomGenerator.nextInt(100);
        int month = _randomGenerator.nextInt(12) + 1;
        int day = _randomGenerator.nextInt(28) + 1; //w uproszczeniu
        int evidenceNumber = _randomGenerator.nextInt(1000);
        int gender = _randomGenerator.nextInt(10);

        String pesel = String.format("%02d%02d%02d%03d%d", year, month, day, evidenceNumber, gender);
        char[] peselElem = pesel.toCharArray();

        int[] weigth = new int[] {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

        int sum = 0;
        for (int i = 0; i < peselElem.length; i++){
            sum += weigth[i] + Integer.parseInt(""+peselElem[i]);
        }

        int checkSum = sum % 10;
        checkSum = checkSum == 0 ? 0 : 10 - checkSum;

        return pesel+checkSum;
    }
}
