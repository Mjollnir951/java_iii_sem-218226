package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Adam on 25.01.2016.
 */
public class ArrayListPersonGenerator implements IGenerator<ArrayList<Person>>{
    private IGenerator<Person> _validPersonGenerator;
    private IGenerator<Person> _invalidPersonGenerator;

    private int _numberOfValid;
    private int _numberOfInvalid;
    private int _numberOfRepeats;

    private ArrayList<Person> persons = new ArrayList<Person>();

    public ArrayListPersonGenerator(IGenerator<Person> validPersonGenerator, IGenerator<Person> invalidPersonGenerator){
        this(validPersonGenerator,invalidPersonGenerator,0,0,0);
    }

    public ArrayListPersonGenerator(IGenerator<Person> validPersonGenerator, IGenerator<Person> invalidPersonGenerator, int numberOfValid, int numberOfInvalid, int repeats){
        _validPersonGenerator = validPersonGenerator;
        _invalidPersonGenerator = invalidPersonGenerator;
        _numberOfValid = numberOfValid;
        _numberOfInvalid = numberOfInvalid;
        _numberOfRepeats = repeats;
    }

    public ArrayList<Person> GenerateNext(){
        persons.clear();
        GenerateValidPart();
        GenerateInvalidPart();
        RandomizeTheList();
        GenerateRepeats();
        RandomizeTheList();
        return persons;
    }

    public void SetValidNumber(int number){
        _numberOfValid = number;
    }

    public void SetInvalidNumber(int number){
        _numberOfInvalid = number;
    }

    public void SetReapetNumber(int number){
        _numberOfRepeats = number;
    }

    private void RandomizeTheList(){
        Collections.shuffle(persons);
    }

    private void GenerateValidPart(){
        for (int i = 0; i < _numberOfValid; i++){
            persons.add(GenerateValid());
        }
    }

    private void GenerateInvalidPart(){
        for (int i = 0; i < _numberOfInvalid; i++){
            persons.add(GenerateInvalid());
        }
    }

    private void GenerateRepeats(){
        Random generator = new Random();

        for (int i = 0; i < _numberOfRepeats; i++){
            persons.add(persons.get(generator.nextInt(persons.size())));
        }
    }

    private Person GenerateValid(){
        return _validPersonGenerator.GenerateNext();
    }

    private Person GenerateInvalid(){
        return _invalidPersonGenerator.GenerateNext();
    }
}
