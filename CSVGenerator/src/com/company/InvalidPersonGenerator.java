package com.company;

/**
 * Created by Adam on 25.01.2016.
 */
public class InvalidPersonGenerator extends PersonGenerator{
    @Override
    protected String GeneratePesel() {
        String pesel = "";

        for(int i = 0; i < 11; i++) {
            pesel += _randomGenerator.nextInt(10);
        }

        return pesel;
    }
}
