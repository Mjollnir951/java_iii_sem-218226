package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by Adam on 25.01.2016.
 */
public class PersonListFileSaver {
    private ArrayList<Person> persons;

    private String fileName = "Persons.CSV";

    public PersonListFileSaver(){
        this(null);
    }

    public PersonListFileSaver(ArrayList<Person> persons){
        this.persons = persons;
    }

    public void SetFileName(String fileName){
        this.fileName = fileName;
    }

    public void SetArrayList(ArrayList<Person> persons){
        this.persons = persons;
    }

    private ArrayList<String> ConvertToStringList(){
        ArrayList<String> stringArrayList = new ArrayList<String>();

        for(Person person : persons){
            stringArrayList.add(person.CovertToString());
        }

        return stringArrayList;
    }

    public void Save(){
        Path file = Paths.get(fileName);
        try {
            Files.write(file, ConvertToStringList(), Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println("Saving " + fileName + "file failed.");
        }
    }
}
