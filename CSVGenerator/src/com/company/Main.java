package com.company;

public class Main {

    public static void main(String[] args) {
	    IGenerator<Person> valid = new PersonGenerator();
        IGenerator<Person> invalid = new InvalidPersonGenerator();
        ArrayListPersonGenerator personGenerator = new ArrayListPersonGenerator(valid, invalid, 4500, 500, 600);
        PersonListFileSaver fileSaver = new PersonListFileSaver(personGenerator.GenerateNext());
        fileSaver.Save();
    }
}
