package com.company;

/**
 * Created by Adam on 25.01.2016.
 */
public class Person {
    public int ID;
    public String Imie;
    public String Nazwisko;
    public String Pesel;

    public Person(int id, String imie, String nazwisko, String pesel){
        ID = id;
        Imie = imie;
        Nazwisko = nazwisko;
        Pesel = pesel;
    }

    public String CovertToString(){
        return String.format("%d, %s, %s, %s", ID, Imie, Nazwisko, Pesel);
    }
}
