import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Created by Adam on 2015-10-18.
 */
public interface Reader {

    void read() throws IOException;
}
