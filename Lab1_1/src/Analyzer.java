import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adam on 2015-10-18.
 */
public class Analyzer {
    public int counter = 0;
    public void find(List<String> file)
    {
        List<String> links = new ArrayList<>();

        for(int i=0;file.size()>i;i++)
        {
            if(file.get(i).contains("<a href=\""))
            {
                counter ++;
                try {
                    links.add(file.get(i).substring(file.get(i).indexOf("<a href=")+9, file.get(i).indexOf("</a>")-4));
                } catch (StringIndexOutOfBoundsException e)
                {
                    try{
                        links.add(file.get(i).substring(file.get(i).indexOf("<a href=")+9, file.get(i).indexOf("\">")-2));
                    }
                    catch (StringIndexOutOfBoundsException ex)
                    {
                        System.out.println("String index is invalid!");
                    }
                }
            }
        }

        int wybor = 0;
        do {
            System.out.println("Co chcesz zrobic ?\n" +
                    "1. Wyswietl zrodlo strony\n" +
                    "2. Wyswietl ilosc linkow\n" +
                    "3. Wyswietl linki\n" +
                    "4. Wyjscie");
            Scanner input = new Scanner(System.in);
            wybor = input.nextInt();

            if(wybor == 1)
            {
                file.forEach(System.out::println);
            }
            else if(wybor == 2)
            {
                System.out.println(counter);
            }
            else if(wybor == 3)
            {
                links.forEach(System.out::println);
            }
        }
        while(wybor != 4);
    }
}
