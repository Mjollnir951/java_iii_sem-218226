import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Adam on 2015-10-18.
 */
public class WebReader implements Reader {

    public void read() throws IOException {
        System.out.println("Podaj zrodlo strony do sprawdzenia: ");
        Scanner input = new Scanner(System.in);
        String sciezka = "";
        sciezka = input.nextLine();
        URL page = new URL(sciezka);
        HttpURLConnection conn = (HttpURLConnection) page.openConnection();
        conn.connect();
        InputStreamReader in = new InputStreamReader((InputStream) conn.getContent(), "UTF-8");

        List<String> web_page = new ArrayList<>();

        BufferedReader buff = new BufferedReader(in);

        for(int i=0; i <= 150; i++)
        {
            web_page.add(buff.readLine());
        }

        Analyzer web = new Analyzer();
        web.find(web_page);



    }
}
