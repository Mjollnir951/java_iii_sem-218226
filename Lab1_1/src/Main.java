/**
 * Created by Adam on 2015-10-18.
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;


public class Main {
    public static void main(String[] args)
    {
        System.out.println("Witaj w programie!");
        Scanner input = new Scanner(System.in);
        int wybor = 0;

        do{
            System.out.println("Chcesz wczytac z dysku czy z adresu URL ?\n" +
                    "1. Dysk\n" +
                    "2. URL\n" +
                    "3. Wyjscie");
            wybor = input.nextInt();

            if (wybor == 1)
            {
                FileReader index = new FileReader();
                try {
                    index.read();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
            else {
                if (wybor == 2) {
                    WebReader example = new WebReader();
                    try {
                        example.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        while (wybor != 3);



    }
}
