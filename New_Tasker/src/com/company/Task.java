package com.company;

/**
 * Created by Adam on 2016-01-26.
 */
public class Task {
    private String category;
    private String content;
    private int priority;
    private boolean done;
    private boolean in_progress;

    public Task(String cat, String cont, int prior, boolean in_prog){
        category = cat;
        content = cont;
        priority = prior;
        in_progress = in_prog;
    }

    String getCategory(){
        return category;
    }

    String getContent(){
        return content;
    }

    int getPriority(){
        return priority;
    }

    boolean isDone(){
        return done;
    }

    boolean isIn_progress(){
        return in_progress;
    }

    public void edit_done(boolean logic_value){
        done = logic_value;
    }
}
