package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adam on 2016-01-26.
 */
public class Add_f extends JFrame implements ActionListener {
    private String[] categories = {"choose", "today", "tomorrow", "this week", "this month"};
    private JComboBox Category= new JComboBox<>(categories);
    private JPanel panel1;
    private JTextPane Content;
    private JTextField Priority;
    private JCheckBox isInProgressCheckBox;
    private JButton addButton;
    private JLabel Enter_Priority;

    public Add_f(){
        setSize(300,400);
        setTitle("Add new");
        setLayout(null);

        Category = new JComboBox(categories);
        add(Category);
        Category.setBounds(5,5,250,30);

        Content = new JTextPane();
        add(Content);
        Content.setBounds(5,40,250,210);

        Enter_Priority = new JLabel("Enter Priority");
        add(Enter_Priority);
        Enter_Priority.setBounds(5,250,250,30);

        Priority = new JTextField("0");
        add(Priority);
        Priority.setBounds(5,290,250,30);

        isInProgressCheckBox = new JCheckBox("Is in progress?");
        add(isInProgressCheckBox);
        isInProgressCheckBox.setBounds(5,330,250,30);

        addButton = new JButton("Add");
        add(addButton);
        addButton.setBounds(5,370,250,30);
        addButton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == addButton ){
            Maganement.add(new Task((String)Category.getSelectedItem(),Content.getText(),Integer.parseInt(Priority.getText()),isInProgressCheckBox.isSelected()));
            this.dispose();
        }
    }
}
