package com.company;

import java.util.ArrayList;

/**
 * Created by Adam on 2016-01-26.
 */
public final class Maganement {
    public static ArrayList<Task> tasks = new ArrayList<>();

    public static void add(Task new_task){
        tasks.add(new_task);
    }

    public static void delete(int number){
        tasks.remove(number);
    }

    public static void delete(String content){
        for (Task t:tasks)
        {
            if(t.getContent().equals(content))
                tasks.remove(t);
        }
    }

    public static void mark_as_done(int number){
        tasks.get(number).edit_done(true);
    }

    public static void mark_as_done(String content){
        for (Task t:tasks)
        {
            if(t.getContent().equals(content))
                t.edit_done(true);
        }
    }

    public static ArrayList<Task> filter_by_category(String category){
        ArrayList<Task> searched = new ArrayList<>();

        for (Task task : tasks){
            if(task.getCategory().equals(category)) {
                searched.add(task);
            }
        }

        return searched;
    }

    public static String[] getFilteredContent(String category){
        String[] contents = new String[filter_by_category(category).size()];
        String[] tmp = {"brak"};

        for (Task t:filter_by_category(category)){
            int i=0;
            contents[i] = t.getContent();
            i++;
        }

        if (contents[0] != null) return contents;
        else return tmp;

    }

    public static String[] getFilteredPriority(String category){
        String[] priorities = new String[filter_by_category(category).size()];
        String[] tmp = {"brak"};

        for (Task t:filter_by_category(category)){
            int i=0;
            priorities[i] = Integer.toString(t.getPriority());
            i++;
        }
        if (priorities[0]!=null) return priorities;
        else return tmp;
    }

    public static String[] getFilteredProgess(String category){
        String[] progress = new String[filter_by_category(category).size()];
        String[] tmp = {"brak"};

        for (Task t:filter_by_category(category)){
            int i=0;
            if (t.isIn_progress()==true) progress[i] = "true";
            else progress[i] = "fasle";
            i++;
        }
        if (progress[0]!=null) return progress;
        else return tmp;
    }

    public static String[] getFilteredDone(String category){
        String[] dones = new String[filter_by_category(category).size()];
        String[] tmp = {"brak"};

        for (Task t:filter_by_category(category)){
            int i=0;
            if (t.isDone()==true) dones[i] = "true";
            else dones[i] = "fasle";
            i++;
        }
        if (dones[0]!=null) return dones;
        else return tmp;
    }
}
