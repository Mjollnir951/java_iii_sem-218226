package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Adam on 2016-01-26.
 */
public class Main_f extends JFrame implements ActionListener {
    private JPanel panel1;
    private JLabel LContent, LPriority, LIn_Progress, LDone;
    private JButton addButton;
    private JButton deleteButton;
    private JButton markDoneButton;
    private JList Priority;
    private JList In_Progress;
    private JList Done;
    private JList Content;
    private String[] categories = {"choose", "today", "tomorrow", "this week", "this month"};
    private JComboBox Choose_Category;
    private DefaultListModel MCont,MPrior,MIn_prog,MDone;

    public void decision(String category){
        for (int i=0;i<Maganement.filter_by_category(category).size();i++)
        {
            MCont.addElement(Maganement.getFilteredContent(category)[i]);
            MPrior.addElement(Maganement.getFilteredPriority(category)[i]);
            MIn_prog.addElement(Maganement.getFilteredProgess(category)[i]);
            MDone.addElement(Maganement.getFilteredDone(category)[i]);
        }

    }

    public Main_f(){
        setSize(600,600);
        setTitle("New Tasker");
        setLayout(null);

        Choose_Category = new JComboBox(categories);
        add(Choose_Category);
        Choose_Category.setBounds(5,5,250,30);
        Choose_Category.addActionListener(this);

        addButton = new JButton("Add");
        add(addButton);
        addButton.setBounds(260,5,100,30);
        addButton.addActionListener(this);

        deleteButton = new JButton("Delete");
        add(deleteButton);
        deleteButton.setBounds(370,5,100,30);
        deleteButton.addActionListener(this);

        markDoneButton = new JButton("Mark as done");
        add(markDoneButton);
        markDoneButton.setBounds(480,5,100,30);
        markDoneButton.addActionListener(this);

        LContent = new JLabel("Content");
        add(LContent);
        LContent.setBounds(5,45,250,30);

        LPriority = new JLabel("Priority");
        add(LPriority);
        LPriority.setBounds(260,45,100,30);

        LIn_Progress = new JLabel("In Progress");
        add(LIn_Progress);
        LIn_Progress.setBounds(370,45,100,30);

        LDone = new JLabel("Done");
        add(LDone);
        LDone.setBounds(480,45,100,30);

        MCont = new DefaultListModel();
        MPrior = new DefaultListModel();
        MIn_prog = new DefaultListModel();
        MDone = new DefaultListModel();

        Content = new JList(MCont);
        add(Content);
        Content.setBounds(5,85,250,400);

        Priority = new JList(MPrior);
        add(Priority);
        Priority.setBounds(260,85,100,400);

        In_Progress = new JList(MIn_prog);
        add(In_Progress);
        In_Progress.setBounds(370,85,100,400);

        Done = new JList(MDone);
        add(Done);
        Done.setBounds(480,85,100,400);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source==Choose_Category){
            Main_f.this.decision((String) Choose_Category.getSelectedItem());
        }
        else if (source == addButton){
            Add_f add_form = new Add_f();
            add_form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            add_form.setVisible(true);
        }
        else if (source == deleteButton){
            Maganement.delete(((String) Content.getSelectedValue()));
        }
        else if (source == markDoneButton){
            Maganement.mark_as_done((String) Content.getSelectedValue());
        }
    }
}
